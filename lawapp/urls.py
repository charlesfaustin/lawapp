from django.conf.urls import patterns, include, url
from lawapp.views import HomePageView
from lawlog.views import (ClientCreateView, ClientSearchListView,
                          ClientDetailView, ClientUpdateView, MatterCreateView,
                          MatterUpdateView, MatterDetailView, MatterListView, 
                          VisitCreateView, logout_link)

from django.contrib import admin
admin.autodiscover()

#from rest_framework import routers

#router = routers.DefaultRouter()
#router.register(r'users', UserViewSet)
#router.register(r'groups', GroupViewSet)
#router.register(r'clients', ClientViewSet)


client_patterns = patterns('',
    url(r'^search/', ClientSearchListView.as_view(), name='client_search' ),
    url(r'^add/', ClientCreateView.as_view(), name='client_add'),
    url(r'^detail/(?P<pk>\d+)/$', ClientDetailView.as_view(), name='client_detail'),
    url(r'^update/(?P<pk>\d+)/$', ClientUpdateView.as_view(), name='client_update'),
)

matter_patterns = patterns('',
    url(r'^add/(?P<client_id>[^/]+)/$', MatterCreateView.as_view(), name='matter_add'),
    url(r'^detail/(?P<pk>\d+)/$', MatterDetailView.as_view(), name='matter_detail'),
    url(r'^update/(?P<pk>\d+)/$', MatterUpdateView.as_view(), name='matter_update'),
    url(r'^list/', MatterListView.as_view(), name='matter_list'),
    url(r'^(?P<filter>\w+)/$', MatterListView.as_view(), name='matter_list_filter'),
)

visit_patterns = patterns('',
    url(r'^add/(?P<matter_id>[^/]+)/$', VisitCreateView.as_view(), name='visit_add'),
    #url(r'^detail/(?P<pk>\d+)/$', VisitDetailView.as_view(), name='visit_detail'),
    #url(r'^update/(?P<pk>\d+)/$', VisitUpdateView.as_view(), name='visit_update'),

)

urlpatterns = patterns('',
    # Examples:
    url(r'^$', HomePageView.as_view(), name='home'),
    #url(r'^ayypi/', include(router.urls)),

    # url(r'^blog/', include('blog.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='account_login'),
    url(r'^accounts/logout/$', logout_link, name ='account_logout'),
    #url(r'^search/', ClientSearchListView.as_view(), name='client_search' ),
    url(r'^case/', include(matter_patterns)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^client/', include(client_patterns)),
    url(r'^visit/', include(visit_patterns)),

    url(r'^admin/', include(admin.site.urls)),
)