import os
import logging

from django.conf import settings

from splinter import Browser
from splinter.request_handler.status_code import HttpResponseError


class SplinterIssueClass(object):
    """ Class for testing web application via Python Splinter """
    LOG_PATH = '%ssplinterlogs/default/splinter.log' % settings.BASE_DIR
    logger = None

    def __init__(self, driver_name, base_url=None, log_path=None):
        self.setup_logger(log_path)
        self.browser = Browser(driver_name)
        self.base_url = base_url if base_url else 'http://localhost:8000/'

    def setup_logger(self, log_path=None):
        try:
            os.remove(log_path if log_path else self.LOG_PATH)
        except OSError:
            pass

        self.logger = logging.getLogger('issue_splinter')
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_path if log_path else self.LOG_PATH)
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        self.logger.addHandler(ch)
        self.logger.addHandler(fh)

    def visit(self, url=None):
        visit_url = '%s%s' % (self.base_url, url) if url else self.base_url
        try:
            self.browser.visit(visit_url)
        except HttpResponseError:
            self.browser.status_code.code = 500
        else:
            self.check_text_http_response_error()
        self.logger.info(
            'Visited %s - Status Code: %s' %
            (visit_url, self.browser.status_code.code)
        )


    def quit_browser(self):
        self.browser.quit()

    def assert_result(self, result, expected_result):
        try:
            assert result == expected_result
        except AssertionError:
            return False
        return True



    def assert_status(self, sttscode):
        status_code = self.browser.status_code.code
        # example status codes are 200,301,302,403,500
        assertion = self.assert_result(status_code, sttscode)
        if assertion:
            self.logger.info('Status Code Valid: %s' % status_code)
        else:
            self.logger.info('Status Code Invalid: %s - Expected: %s' % (status_code, sttscode))
        return assertion

    def check_current_url(self, path=None):
        
        self.logger.info('current url is : %s, should be at %s' % (self.browser.url, self.base_url + path))
        return self.base_url + path == self.browser.url


    def assert_text_is_present(self, text, page):
        assertion = True
        if isinstance(text, list):
            if not text:
                assertion = False
            for item in text:
                if not self.assert_result(self.browser.is_text_present(item), True):
                    self.logger.info('Text Not Found: %s - Page: %s' % (item, page))
                    assertion = False
                else:
                    self.logger.info('Text Found: %s - Page: %s' % (item, page))
        else:
            assertion = self.assert_result(self.browser.is_text_present(text), True)
            if assertion:
                self.logger.info('Text Found: %s - Page: %s' % (text, page))
            else:
                self.logger.info('Text Not Found: %s - Page: %s' % (text, page))
        return assertion

    def fill_form(self, form_name_values):
        for name, value in form_name_values.iteritems():
            self.browser.fill(name, value)
            self.logger.info('Form with name  %s filled ' % (name))

    def check_box(self, checkbox_name):
        self.browser.check(checkbox_name)
        self.logger.info('Checkbox with name  %s checked ' % (checkbox_name))

    def uncheck_box(self, checkbox_name):
        self.browser.uncheck(checkbox_name)
        self.logger.info('Checkbox with name  %s unchecked ' % (checkbox_name))

    # selects first check box with a particular value
    def check_box_by_value(self, val):
        self.browser.find_by_value(val).first.check()
        self.logger.info('Check box with value  %s checked ' % (val))

    def choose_radio(self, name, value):
        self.browser.choose(name, value)
        self.logger.info('Radio button with name  %s and value %s chosen ' % (name, value))

    def submit_form(self, submit_button_name=None, submit_button_id='submit'):
        if submit_button_name:
            self.browser.find_by_name(submit_button_name).click()
        else:
            self.browser.find_by_id(submit_button_id).click()
        self.logger.info('Form with id  %s submitted ' % (submit_button_id))

    def find_link_by_href(self, href):
        self.browser.find_link_by_href(href).first.click()

    def find_by_value(self, value):
        self.browser.find_by_value(value)
        self.logger.info('Item with value  %s found ' % (value))

    def execute_script(self, script):
        self.browser.execute_script(script)

    def select_option(self, name, value):
        self.browser.select(name, value)
        self.logger.info('option %s selected ' % (value))
        """
        Selects an ``<option>`` element in an ``<select>`` element using the ``name`` of the ``<select>`` and
        the ``value`` of the ``<option>``.

        Example:

            >>> browser.select("state", "NY")
        """

    def click_link(self, find_by, value):
        find_by_types = {
            '_id': getattr(self.browser, 'find_by_id'),
            '_name': getattr(self.browser, 'find_by_name')
        }
        x = find_by_types.get(find_by, '_id')
        x(value).click()
        self.logger.info(
            'Visited %s - Status Code: %s' %
            (self.browser.url, self.browser.status_code.code)
        )
        self.check_text_http_response_error()

    def check_text_http_response_error(self):
        # Useful in debug mode. Sets browser status code to 500 if True.
        if self.assert_text_is_present('Error', self.browser.url):
            self.browser.status_code.code = 500

    def login(self, username, password):
        self.visit('accounts/login/')
        self.fill_form(
            {
                'username': username,
                'password': password
            }
        )
        self.submit_form(submit_button_id='loginButton')
        self.logger.info(
            'Logged In - Username: %s - Password: %s - Status Code: %s' %
            (username, password, self.browser.status_code.code)
        )

    def logout(self):
        self.visit('accounts/logout/')
        self.logger.info('Logged Out - Status Code: %s' % self.browser.status_code.code)
