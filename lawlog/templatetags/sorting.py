from django import template

register = template.Library()


@register.inclusion_tag('sort_by.html', takes_context=True)
def sort_by(context, field_slug):
    context.update({
        'field_slug': field_slug,
        'field_verbose': field_slug.replace('_', ' ').capitalize(),
    })
    return context
