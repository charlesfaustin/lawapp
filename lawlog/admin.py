from django.contrib import admin
from .models import Client, Matter, Visit
# Register your models here.


class ClientAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'first_name', 'surname',
                    'email_address', 'landline_number', 'mobile_number')
    list_filter = ('first_name', 'surname', 'email_address', 'landline_number', 'mobile_number')


class MatterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','client', 'title', 'date_created')
    list_filter = ('date_created', 'client', 'title', 'date_created')

admin.site.register(Client, ClientAdmin)
admin.site.register(Matter, MatterAdmin)
admin.site.register(Visit)