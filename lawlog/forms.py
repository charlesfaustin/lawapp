
from django import forms
from .models import Matter, Visit


class ClientSearchForm(forms.Form):
    first_name = forms.CharField(required=False,
                                 label='Search by first name ',
                                 widget=forms.TextInput(attrs={'placeholder':
                                                        'enter all/part of'
                                                        ' the first name'})
                                 )

    surname = forms.CharField(required=False,
                              label='Search by surname ',
                              widget=forms.TextInput(attrs={'placeholder':
                                                     'enter all/part of'
                                                     ' the surname'})
                              )

    primary_key = forms.IntegerField(required=False,
                                     label='client id ',
                                     min_value=1,
                                     widget=forms.TextInput(attrs={'placeholder':
                                                            'e.g to find 00005, enter "5"'})
                                     )


class MatterCreateForm(forms.ModelForm):

    class Meta:
        model = Matter
        exclude = ('client', 'ordered_num')
        labels = {
            'title': 'Select Case Type/Title',
        }


class VisitCreateForm(forms.ModelForm):

    class Meta:
        model = Visit
        exclude = ('case',)
        labels = {
            'additional_notes': 'Notes',
        }