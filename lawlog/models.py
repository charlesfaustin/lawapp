from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.

REFERRAL_CHOICES = (
    ('friend', 'From a Friend'),
    ('family', 'From a family member'),
    ('work', 'From my workplace'),
    ('council', 'From my local council'),
    ('other', 'other'),
)

MATTER_TITLE_CHOICES = (
    ('tax', 'Tax'),
    ('Family', 'Family'),
    ('Housing', 'Housing'),
    ('Asylum/Immigration', 'Asylum/Immigration'),
    ('Civil litigation', 'Civil litigation'),
    ('Consumer', 'Consumer'),
    ('Contract', 'Contract'),
    ('Crime', 'Crime'),
    ('Debt', 'Debt'),
    ('Employment', 'Employment'),
    ('Human Rights', 'Human Rights'),
    ('Welfare Benefits', 'Welfare Benefits'),
    ('Other', 'Other'),
)


PERSON_TITLE_CHOICES = (
    ('mr', 'Mr'),
    ('ms', 'Ms'),
    ('mrs', 'mrs')
)


class Client(models.Model):
    first_name = models.CharField(max_length=50, blank=True,
                                  null=True, verbose_name=u'First name')

    surname = models.CharField(max_length=50, blank=True,
                               null=True, verbose_name=u'surname')

    title = models.CharField(max_length=20, blank=True,
                             null=True, choices=PERSON_TITLE_CHOICES,
                             verbose_name=u'title')

    first_address_line = models.CharField(max_length=40, blank=True,
                                          null=True,
                                          verbose_name=u'1st line of address')

    second_address_line = models.CharField(max_length=40, blank=True,
                                           null=True,
                                           verbose_name=u'2nd line of address')

    third_address_line = models.CharField(max_length=40, blank=True,
                                          null=True,
                                          verbose_name=u'3rd line of address')

    postcode = models.CharField(max_length=10, blank=True, null=True,
                                verbose_name=u'Postcode')

    town = models.CharField(max_length=30, blank=True, null=True,
                            verbose_name=u'Town')

    country = models.CharField(max_length=30, blank=True, null=True,
                               verbose_name=u'Country')


    landline_number = models.CharField(max_length=20, blank=True, null=True,
                                       verbose_name=u'Landline Phone Number')

    mobile_number = models.CharField(max_length=20, blank=True, null=True,
                                     verbose_name=u'Mobile Phone Number')

    email_address = models.CharField(max_length=50, blank=True, null=True,
                                     verbose_name=u'Email Address')
    referrer = models.CharField(blank=True, null=True, max_length=10,
                                choices=REFERRAL_CHOICES,
                                verbose_name=(u'How did you find out about'
                                              ' our  service?'))
    additional_info = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return 'Client Number %06d' % self.pk

    def get_absolute_url(self):
        return reverse('client_detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ["-id"]
        verbose_name = u'Client'
        verbose_name_plural = u'Clients'


class Matter(models.Model):
    client = models.ForeignKey('lawlog.Client', related_name='matter',
                               verbose_name=u'Client')

    title = models.CharField(max_length=20, blank=True,
                             null=True, choices=MATTER_TITLE_CHOICES)
    date_created = models.DateTimeField('date created', auto_now_add=True)

    notes = models.TextField(null=True, blank=True)

    ordered_num = models.IntegerField(blank=True, null=True)

    #def save(self, *args, **kwargs):
    #    if not self.ordered_num:
    #        print len(Matter.objects.filter(client=self.client))
    #    
    #    super(Matter, self).save(*args, **kwargs) # Call the "real" save() 

    def __unicode__(self):
        return '%s, Case ID %03d' % (self.client, self.ordered_num)

    def get_absolute_url(self):
        return reverse('matter_detail', kwargs={'pk': self.pk})


class Visit(models.Model):
    date_of_visit = models.DateTimeField('Date of visit', auto_now_add=True)

    case = models.ForeignKey('lawlog.Matter',
                             related_name='visit')

    additional_notes = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('visit_detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return 'Case number %03d,  %s' % ( self.case.ordered_num, self.date_of_visit.strftime('%A %d %B %Y'))

