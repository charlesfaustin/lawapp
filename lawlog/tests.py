from django.test import TestCase
from lawapp.splinterclass import SplinterIssueClass
from lawapp import settings
from .models import Client, Matter, Visit
from django.contrib.auth.models import User


# Create your tests here.
class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

class TestCreateDeleteUser(TestCase):
    def test_create_user(self):
        """
        Tests that a standard django user can be created and deleted
        """
        self.newuser = User.objects.create_user('john',
                                                'lennon@thebeatles.com',
                                                'johnpassword')
        self.newuser.delete()


class CreateDeleteClientMatterVisit(TestCase):
    def test(self):
        """
        Tests that a client, matter & visit can be created
        and then deleted
        """
        self.blankclient = Client.objects.create()
        self.matter = Matter.objects.create(client=self.blankclient,
                                            title='tax')
        self.visit = Visit.objects.create(case=self.matter,
                                          additional_notes="hello")

        self.visit.delete()
        self.matter.delete()
        self.blankclient.delete()


class TestWithSplinter(TestCase):
    """
    Splinter test to check visual elements
    """
    def setUp(self):
        self.issue_splinter = SplinterIssueClass
        self.vs = None

    def single_customer_user(
            self, base_url, username, password, driver_name='phantomjs',
            log_path='%s/lawapp/splinterlogs/default/splinter.log' % settings.BASE_DIR):
        self.vs = self.issue_splinter(driver_name,
                                      base_url=base_url, log_path=log_path)
        self.vs.logger.info(
            'Running General Splinter test for the project as username:'
            ' %s with driver name: %s' % (username, driver_name)
        )

        """visits admin page first"""
        self.vs.visit('admin')


        #new test func
        self.assertTrue(self.vs.check_current_url('admin/'), 'wronggg')


        self.assertTrue(self.vs.assert_status(301),
                        'Admin URL did not return status code 301')

        """visits homepage"""
        self.vs.visit()
        self.assertTrue(self.vs.assert_status(200),
                        'Home URL did not return status code 200')


        """goes to the add client page, ClientCreateView"""
        self.vs.visit('client/add')
        self.assertTrue(self.vs.assert_status(301),
                        'New Client URL did not return status code 301')
        self.vs.fill_form({'username':'charles', 'password':'lululu1' })
        self.vs.submit_form()
        """ Fills in new client form and submits it"""
        self.vs.fill_form({'first_name': 'first name',
                           'full_initials': 'INT',
                           'surname': 'somesurname', 'title': 'Sir',
                           'first_address_line': '200 some place',
                           'second_address_line': 'over there',
                           'postcode': 'wtv3v1',
                           'town': 'sometown', 'country': 'UK',
                           'additional_info': 'hello',
                           'phone_number': '07777777777',
                           'email_address': 'name@domain.com',
                           })
        self.vs.select_option('local_authority', 'authority2')
        self.vs.select_option('referrer', 'work')
        self.vs.submit_form()

        """checks the client was created successfully"""
        self.assertTrue(self.vs.assert_text_is_present('Client created successfully.',
                                                       'client detail page'))
        self.assertTrue(self.vs.assert_status(301),
                        'Client Detail URL did not return status code 301')

        """Client update page"""
        self.vs.visit('client/update/10')
        self.vs.fill_form({'first_name': 'another name',
                           'full_initials': 'initials',
                           'surname': 'surname', 'title': 'Mr',
                           'first_address_line': '300 nice place',
                           'second_address_line': 'here',
                           'postcode': 'rty tuh',
                           'town': 'paris', 'country': 'France',
                           'additional_info': 'bloop',
                           'phone_number': '0575757575',
                           'email_address': 'dave@place.com',
                           })
        self.vs.select_option('local_authority', 'authority1')
        self.vs.select_option('referrer', 'council')
        self.vs.submit_form()
        self.assertTrue(self.vs.assert_status(301),
                        'Client Update submission did not return status code 301')
        self.assertTrue(self.vs.assert_text_is_present('Client updated successfully.',
                                                       'Client detail page'))



        """client search page"""
        self.vs.visit('client/search')
        self.vs.check_current_url('client/search')

        """ check  created user info is present, and filter search works"""
        self.vs.assert_text_is_present('somesurname', 'client search page')
        self.vs.fill_form({'surname': 'dededdqd'})
        self.vs.submit_form()
        self.vs.check_current_url('client/search')
        self.assertFalse(self.vs.assert_text_is_present('somesurname',
                                                        'client search page'))

        """case update page"""
        self.vs.visit('case/update/10')
        self.assertTrue(self.vs.assert_status(301),
                        'Case Update URL did not return status code 301')
        self.vs.select_option('title', 'tax')
        self.vs.submit_form()
        self.assertTrue(self.vs.assert_status(301),
                        'Matter Update submission did not return status code 301')
        self.assertTrue(self.vs.assert_text_is_present('Case updated successfully.',
                                                       'Case detail page'))

        
        """visit update"""
        self.vs.visit('visit/update/4')
        self.assertTrue(self.vs.assert_status(301),
                        'Visit Update URL did not return status code 301')
        self.vs.fill_form({'additional_notes': 'nothing to add'})
        self.vs.submit_form()
        self.assertTrue(self.vs.assert_status(301),
                        'Visit Update submission did not return status code 301')
        self.assertTrue(self.vs.assert_text_is_present('Visit updated successfully.',
                                                       'Visit detail page'))

        #current test im working on
        """case list page"""
        self.vs.visit('case/list')
        self.assertTrue(self.vs.assert_status(301),
                        'Matter List URL did not return status code 301')


    def test_single_customer_user(self):

        self.single_customer_user(
            'http://localhost:8000/', 'charles', 'lululu1',
        )

    def tearDown(self):
        if self.vs:
            try:
                self.vs.quit_browser()
            except:
                pass
