# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Client'
        db.create_table(u'lawlog_client', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('surname', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('first_address_line', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('second_address_line', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('third_address_line', self.gf('django.db.models.fields.CharField')(max_length=40, null=True, blank=True)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('town', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('landline_number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('mobile_number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('email_address', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('referrer', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('additional_info', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'lawlog', ['Client'])

        # Adding model 'Matter'
        db.create_table(u'lawlog_matter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='matter', to=orm['lawlog.Client'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('notes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('ordered_num', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'lawlog', ['Matter'])

        # Adding model 'Visit'
        db.create_table(u'lawlog_visit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date_of_visit', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('case', self.gf('django.db.models.fields.related.ForeignKey')(related_name='visit', to=orm['lawlog.Matter'])),
            ('additional_notes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'lawlog', ['Visit'])


    def backwards(self, orm):
        # Deleting model 'Client'
        db.delete_table(u'lawlog_client')

        # Deleting model 'Matter'
        db.delete_table(u'lawlog_matter')

        # Deleting model 'Visit'
        db.delete_table(u'lawlog_visit')


    models = {
        u'lawlog.client': {
            'Meta': {'ordering': "['-id']", 'object_name': 'Client'},
            'additional_info': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'email_address': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'first_address_line': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'landline_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'mobile_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'referrer': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'second_address_line': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'third_address_line': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'lawlog.matter': {
            'Meta': {'object_name': 'Matter'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'matter'", 'to': u"orm['lawlog.Client']"}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ordered_num': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'lawlog.visit': {
            'Meta': {'object_name': 'Visit'},
            'additional_notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'case': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'visit'", 'to': u"orm['lawlog.Matter']"}),
            'date_of_visit': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['lawlog']