from django.contrib import messages
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from django.contrib.auth import logout
from .forms import ClientSearchForm, MatterCreateForm, VisitCreateForm
from .models import Client, Matter, Visit
from django.core.urlresolvers import reverse
from lawapp.mixins import SearchListView, UpdateMixin, SortMixin, FilterMixin, LoggedInMixin
# Create your views here.
from django.shortcuts import render
from django.contrib.auth.models import User, Group





class ClientCreateView(CreateView, LoggedInMixin):
    model = Client

    def form_valid(self, form):
        messages.success(self.request, 'Client created successfully.')
        return super(ClientCreateView, self).form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()


class ClientSearchListView(SearchListView, LoggedInMixin):
    model = Client
    template_name = "lawlog/search_form.html"
    form_class = ClientSearchForm
    paginate_by = 10
    search_fields = {
        'surname': ['surname'],
        'first_name': ['first_name'],
        'primary_key': {'operator': '__exact', 'fields': ['pk']},

    }


class ClientDetailView(DetailView, LoggedInMixin):
    model = Client
    
    def get_context_data(self, *args, **kwargs):
        context = super(ClientDetailView, self).get_context_data(**kwargs)
        context.update({
            "matterform": MatterCreateForm,
            
        })
        return context

class ClientUpdateView(UpdateView, UpdateMixin, LoggedInMixin):
    model = Client
    def form_valid(self, form):
        messages.success(self.request, 'Client updated successfully.')
        return super(ClientUpdateView, self).form_valid(form)


class MatterCreateView(CreateView, LoggedInMixin):
    model = Matter
    form_class = MatterCreateForm

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save(commit=False)

        self.client_id = int(self.kwargs.get('client_id'))
        self.object.client = Client.objects.get(pk=self.client_id)

        if not self.object.ordered_num:
            order_num = len(Matter.objects.filter(client=self.object.client))
            self.object.ordered_num = order_num + 1

        self.object = form.save()

        messages.success(self.request, 'Case created successfully.')
        return super(MatterCreateView, self).form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_context_data(self, *args, **kwargs):
        context = super(MatterCreateView, self).get_context_data(**kwargs)
        context.update({
            "client_id": int(self.kwargs.get('client_id')),

        })
        return context


class MatterDetailView(DetailView, LoggedInMixin):
    model = Matter

    def get_context_data(self, *args, **kwargs):
        context = super(MatterDetailView, self).get_context_data(**kwargs)
        context.update({
            "visitform": VisitCreateForm,


        })
        return context


class MatterUpdateView(UpdateView, UpdateMixin, LoggedInMixin):
    model = Matter
    form_class = MatterCreateForm
    def form_valid(self, form):
        messages.success(self.request, 'Case updated successfully.')
        return super(MatterUpdateView, self).form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super(MatterUpdateView, self).get_context_data(**kwargs)
        context.update({
            "client_id": int(self.kwargs.get('pk')),
        })
        return context


class MatterListView(SortMixin, FilterMixin,
                              ListView, LoggedInMixin):
    """A list of background checks viewed from the "Backgroud Check" tab

    Can be sorted by all column headers using the SortMixin and GET
    variables, or filtered using separate URL entry points and the
    FilterMixin.

    """

    model = Matter
    default_filter_param = 'all'
    default_sort_params = ('created', 'desc')
    paginate_by = 10
    template_name = 'lawlog/matter_list.html'
    #required_permissions = (
    #    'core.view_backgroundcheck',
    #)
    
    def sort_queryset(self, qs, sort_by, order):
        """SortMixin override"""

        key = lambda x: x
        if sort_by == 'title':
            # Despite ordering by the reference number, use the started
            # attribute as it should produce the same result.
            key = lambda x: x.title

        if sort_by == 'client':
            key = lambda x: x.client

        if sort_by == 'ID Number':
            key = lambda x: x.pk

        if sort_by == 'created':
            key = lambda x: x.date_created

        qs = sorted(qs, key=key)

        if order == 'desc':
            qs = list(reversed(qs))

        return qs

    def filter_queryset(self, qs, filter_param):
        """FilterMixin override"""

        #line below will filter out other customers cases in the future
        #qs = qs.filter(workflow__brand__in=self.profile.get_brands())
        
        if filter_param != 'all':
            qs = qs.filter(title=filter_param)


        return qs.select_related()



#class VisitDetailView(DetailView, LoggedInMixin):
#    model = Visit


class VisitCreateView(CreateView, LoggedInMixin):
    model = Visit
    form_class = VisitCreateForm

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save(commit=False)

        self.case = int(self.kwargs.get('matter_id'))
        self.object.case = Matter.objects.get(pk=self.case)

        self.object = form.save()

        messages.success(self.request,
                         'Visit number %05d logged '
                         'successfully.' % self.object.pk)
        return super(VisitCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('matter_detail', kwargs={'pk': self.case})

    def get_context_data(self, *args, **kwargs):
        matter = Matter.objects.get(pk=int(self.kwargs.get('matter_id')))
        context = super(VisitCreateView, self).get_context_data(**kwargs)
        context.update({
            "case_id": matter.ordered_num,
            "client_id": matter.client.id
        })
        return context
 


#class VisitUpdateView(UpdateView, UpdateMixin, LoggedInMixin):
#    model = Visit
#    form_class = VisitCreateForm
#    def form_valid(self, form):
#        messages.success(self.request, 'Visit updated successfully.')
#        return super(VisitUpdateView, self).form_valid(form)
#
#    def get_context_data(self, *args, **kwargs):
#        matter = Matter.objects.get(pk=self.object.case.pk)
#        context = super(VisitUpdateView, self).get_context_data(**kwargs)
#        context.update({
#            "case_id": matter.ordered_num,
#            "client_id": matter.client.id
#        })
#        return context


def logout_link(request):
    logout(request)
    return render(request, 'lgt_pg.html')





#
#