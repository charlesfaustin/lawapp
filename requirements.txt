Django==1.6.5
South==1.0
django-debug-toolbar==1.2.1
selenium==2.42.1
splinter==0.6.0
sqlparse==0.1.11
wsgiref==0.1.2
